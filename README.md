# /!\ NOT READY FOR PRODUCTION /!\

not enough tested , folder's rights are not setup properly and security tokens are logged in console and leave in logs file forever...
I'm not enough confident with the warp10 read and write tokens and i don't really know how to display them to the user in the cloudron way.
If you have any idea to help your welcome. 


# Warp10 Cloudron App

This repository contains the Cloudron app package source for [Warp10](https://www.warp10.io/).
It Bundle a Warp10 geo-timeserie database and the WarpStudio web platform


## Usage

after the first install check the logs to get your read and write tokens

## todo

- post install message
- clean the dockerfile
- fix rights
- fix token generation
- admin ui to create, read and delete reads/writes tokens
- stop displaying initial tokens in console
