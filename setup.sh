#!/usr/bin/env bash
#
#   Copyright 2018  SenX S.A.S.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

set -euo pipefail

# WARP10 - install and manage upgrade

if ! [ -f /app/data/.w10initialized ]; then
  echo "Install Warp 10™"
  ${WARP10_HOME}/bin/warp10-standalone.init bootstrap
  touch /app/data/.w10initialized
else
  echo "Warp10 already installed"
fi

# Sensision install
#  ---- deactivate sensision
# if ! [ -f /app/data/.sensisioninitialized ]; then
#   echo "Install Sensision"
#   # Stop/start to init config
#   ${SENSISION_HOME}/bin/sensision.init bootstrap
#   touch /app/data/.sensisioninitialized
# else
#   echo "Sensision already installed"
# fi
# #  ----

#  ----
# chown -Rf cloudron:cloudron ${WARP10_DATA_DIR}
# chown -Rf cloudron:cloudron ${SENSISION_DATA_DIR}
#  ----