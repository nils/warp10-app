FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code /app/data

WORKDIR /app/data


RUN apt-get update && \
    apt-get install -y ca-certificates openjdk-8-jre-headless openjdk-8-jdk less python fontconfig unifont wget
USER root

# Define environement variable used by wap10 scripts
ENV JAVA_HOME=/usr \
  WARP10_VOLUME=/app/data \
  WARP10_HOME=/app/code/warp10home \
  WARP10_DATA_DIR=/app/data/warp10 \
  SENSISION_HOME=/app/data/sensisionhome \
  SENSISION_DATA_DIR=/app/data/sensision \
  CONF_DIR=/app/data/conf \
  WARP10_USER=cloudron \
  WARP10_GROUP=cloudron \
  SENSISION_USER=cloudron \
  SENSISION_VERSION=1.0.19 \
  SENSISION_GROUP=cloudron 

ENV WARP10_CONFIG_DIR=${WARP10_HOME}/etc/conf.d 

ENV WARP10_JAR=${WARP10_HOME}/bin/warp10-${WARP10_VERSION}.jar \
  WARP10_MACROS=${WARP10_VOLUME}/custom_macros \
  WARP10_SECRETS=${WARP10_CONFIG_DIR}/00-secrets.conf \
  SENSISION_JAR=${SENSISION_HOME}/bin/sensision-${SENSISION_VERSION}.jar

# add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/warp10.conf /etc/nginx/sites-available/warp10
RUN ln -s /etc/nginx/sites-available/warp10 /etc/nginx/sites-enabled/warp10

# add supervisor config
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/warp10/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/code/

# Ensure directories
RUN mkdir -p $CONF_DIR $WARP10_VOLUME $WARP10_HOME $SENSISION_HOME $SENSISION_DATA_DIR $WARP10_DATA_DIR
RUN chown ${WARP10_USER}:${WARP10_GROUP} ${WARP10_DATA_DIR}
RUN chmod 750 ${WARP10_DATA_DIR}

# Download warp10
ARG WARP10_VERSION=2.1.0
ARG WARP10_URL=https://dl.bintray.com/senx/generic/io/warp10/warp10/${WARP10_VERSION}
ARG WARPSTUDIO_VERSION=1.0.0
ARG WARPSTUDIO_URL=https://dl.bintray.com/senx/maven/io/warp10/warp10-plugin-warpstudio/${WARPSTUDIO_VERSION}

RUN wget -q ${WARP10_URL}/warp10-${WARP10_VERSION}.tar.gz 
    RUN tar xzf warp10-${WARP10_VERSION}.tar.gz --strip 1 -C ${WARP10_HOME} 
    RUN rm warp10-${WARP10_VERSION}.tar.gz 
    RUN wget -q -P ${WARP10_HOME}/lib ${WARPSTUDIO_URL}/warp10-plugin-warpstudio-${WARPSTUDIO_VERSION}.jar 
    RUN chown -h cloudron:cloudron ${WARP10_HOME}

# Download sensision
ARG SENSISION_URL=https://dl.bintray.com/senx/generic/io/warp10/sensision-service/${SENSISION_VERSION}

RUN wget -q $SENSISION_URL/sensision-service-${SENSISION_VERSION}.tar.gz \
    && tar xzf sensision-service-${SENSISION_VERSION}.tar.gz --strip 1 -C ${SENSISION_HOME} \
    && rm sensision-service-${SENSISION_VERSION}.tar.gz \
    && chown -h cloudron:cloudron ${SENSISION_HOME}
    # && adduser -D -s -H -h ${SENSISION_HOME} -s /bin/bash sensision \


COPY warp10.start.sh ${WARP10_HOME}/bin/warp10.start.sh
COPY setup.sh ${WARP10_HOME}/bin/setup.sh

RUN chmod +x ${WARP10_HOME}/bin/setup.sh
RUN chmod +x ${WARP10_HOME}/bin/warp10.start.sh

ENV PATH=$PATH:${WARP10_HOME}/bin

# setting cloudron user instead of warp10
run sed -i "s~WARP10_USER=warp10~WARP10_USER=${WARP10_USER}~" ${WARP10_HOME}/bin/warp10-standalone.init

# add warp10 symlink
RUN  rsync -a ${WARP10_HOME}/etc/ ${WARP10_DATA_DIR}/etc/ &&\
  rsync -a ${WARP10_HOME}/leveldb/ ${WARP10_DATA_DIR}/leveldb/ &&\
  rsync -a ${WARP10_HOME}/macros/ ${WARP10_DATA_DIR}/macros/ &&\
  rsync -a ${WARP10_HOME}/warpscripts/ ${WARP10_DATA_DIR}/warpscripts/ && \
  rsync -a ${WARP10_HOME}/logs/ /run/warp10/ && \
  rsync -a ${WARP10_HOME}/jars/ ${WARP10_DATA_DIR}/jars/ && \
  rsync -a ${WARP10_HOME}/lib/ ${WARP10_DATA_DIR}/lib/ && \
  rsync -a ${WARP10_HOME}/datalog/ /run/datalog/ && \
  rsync -a ${WARP10_HOME}/datalog_done/ /run/datalog_done/ && \
  rsync -a ${WARP10_HOME}/templates/ $WARP10_DATA_DIR/templates/ && \
  rm -rf ${WARP10_HOME}/etc/ && \
  rm -rf ${WARP10_HOME}/leveldb && \
  rm -rf ${WARP10_HOME}/macros && \
  rm -rf ${WARP10_HOME}/warpscripts && \
  rm -rf ${WARP10_HOME}/logs && \
  rm -rf ${WARP10_HOME}/jars && \
  rm -rf ${WARP10_HOME}/lib && \
  rm -rf ${WARP10_HOME}/datalog && \
  rm -rf ${WARP10_HOME}/datalog_done && \
  rm -rf ${WARP10_HOME}/templates && \
  ln -s ${WARP10_DATA_DIR}/etc ${WARP10_HOME}/etc && \
  ln -s ${WARP10_DATA_DIR}/leveldb ${WARP10_HOME}/leveldb && \
  ln -s ${WARP10_DATA_DIR}/macros ${WARP10_HOME}/macros && \
  ln -s ${WARP10_DATA_DIR}/warpscripts ${WARP10_HOME}/warpscripts && \
  ln -s /run/warp10/ ${WARP10_HOME}/logs && \
  ln -s ${WARP10_DATA_DIR}/jars ${WARP10_HOME}/jars && \
  ln -s ${WARP10_DATA_DIR}/lib ${WARP10_HOME}/lib && \
  ln -sf /run/datalog ${WARP10_HOME}/datalog && \
  ln -sf /run/datalog_done ${WARP10_HOME}/datalog_done && \
  ln -s ${WARP10_DATA_DIR}/templates ${WARP10_HOME}/templates

# /app/code/warp10home/logs/warp10.log (Read-only file system)
# add sensision symlink
RUN rsync -a ${SENSISION_HOME}/etc/ ${SENSISION_DATA_DIR}/etc/ && \
  rsync -a ${SENSISION_HOME}/scripts/ ${SENSISION_DATA_DIR}/scripts/ && \
  rsync -a ${SENSISION_HOME}/logs/ /run/sensision/ && \
  rsync -a ${SENSISION_HOME}/metrics/ ${SENSISION_DATA_DIR}/metrics/ && \
  rsync -a ${SENSISION_HOME}/targets/ ${SENSISION_DATA_DIR}/targets/ && \
  rsync -a ${SENSISION_HOME}/queued/ ${SENSISION_DATA_DIR}/queued/ && \ 
  mkdir -p ${SENSISION_DATA_DIR}/bin/sensision.init && \
  rm -rf /etc/init.d/sensision ${SENSISION_HOME}/etc ${SENSISION_HOME}/scripts ${SENSISION_HOME}/logs ${SENSISION_HOME}/metrics ${SENSISION_HOME}/targets ${SENSISION_HOME}/queued && \
  ln -sf ${SENSISION_DATA_DIR}/etc ${SENSISION_HOME}/etc && \
  ln -sf ${SENSISION_DATA_DIR}/scripts ${SENSISION_HOME}/scripts && \
  ln -sf /run/sensision ${SENSISION_HOME}/logs && \
  ln -sf ${SENSISION_DATA_DIR}/metrics ${SENSISION_HOME}/metrics && \
  ln -sf ${SENSISION_DATA_DIR}/targets ${SENSISION_HOME}/targets && \
  ln -sf ${SENSISION_DATA_DIR}/queued ${SENSISION_HOME}/queued && \
  ln -sf ${SENSISION_DATA_DIR}/bin/sensision.init /etc/init.d/sensision

# replace script and conf file with modified one for cloudron
RUN rm ${WARP10_HOME}/bin/warp10-standalone.sh
COPY warp10/warp10-standalone.sh ${WARP10_HOME}/bin/warp10-standalone.sh
RUN chmod +x ${WARP10_HOME}/bin/warp10-standalone.sh

RUN rm ${WARP10_HOME}/etc/log4j.properties
COPY log4j.properties ${WARP10_HOME}/etc/log4j.properties

RUN rm ${SENSISION_HOME}/templates/log4j.properties.template
COPY log4j.properties.template ${SENSISION_HOME}/template/log4j.properties.template

# fix permission here instead of /warp10-standalone.sh
RUN echo "Fix permissions.." && \
  chmod 750 ${WARP10_HOME} && \
  chmod 755 ${WARP10_HOME}/bin && \
  chmod 755 ${WARP10_HOME}/etc && \
  chmod 755 ${WARP10_HOME}/logs && \
  chmod 755 ${WARP10_HOME}/macros && \
  chmod 755 ${WARP10_HOME}/jars && \
  chmod 755 ${WARP10_HOME}/lib && \
  chmod 755 ${WARP10_HOME}/templates && \
  chmod 755 ${WARP10_HOME}/warpscripts && \
  chmod 755 ${WARP10_HOME}/etc/throttle && \
  chmod 755 ${WARP10_HOME}/etc/trl && \
  chmod 755 ${WARP10_HOME}/etc/bootstrap && \
  chmod 644 ${WARP10_HOME}/etc/bootstrap/*.mc2 && \
  chmod 755 ${WARP10_HOME}/bin/*.sh && \
  chmod 755 ${WARP10_HOME}/bin/*.init && \
  chmod 644 ${WARP10_HOME}/bin/*.service && \
  chmod 644 ${WARP10_HOME}/bin/warp10-2.1.0.jar && \
  chmod -R 755 ${WARP10_HOME}/templates && \
  chmod -R 755 ${WARP10_HOME}/datalog && \
  chmod -R 755 ${WARP10_HOME}/datalog_done && \
  chmod -R ugo+rwx ${WARP10_HOME}/leveldb  
  # chmod -R 755 ${WARP10_HOME}/leveldb

  


#sensision init script 
RUN  chown -R ${SENSISION_USER}:${SENSISION_GROUP} ${SENSISION_HOME}/

RUN echo ${SENSISION_JAR}
RUN echo ${SENSISION_HOME}
RUN echo ${SENSISION_VERSION}


RUN chmod 775 ${SENSISION_HOME} && \
  chmod 775 ${SENSISION_HOME}/bin && \
  chmod 775 ${SENSISION_HOME}/etc && \
  chmod 775 ${SENSISION_HOME}/templates && \
  chmod 775 ${SENSISION_HOME}/logs && \
  chmod 1733 ${SENSISION_HOME}/targets && \
  chmod 1733 ${SENSISION_HOME}/metrics && \
  chmod 700 ${SENSISION_HOME}/queued && \
  chmod -R 775 ${SENSISION_HOME}/scripts && \
  chmod 664 ${SENSISION_HOME}/bin/sensision-${SENSISION_VERSION}.jar && \
  chmod 775 ${SENSISION_HOME}/bin/sensision.init

RUN chown root:cloudron ${SENSISION_HOME}/bin/procDump && \
  chmod 4750 ${SENSISION_HOME}/bin/procDump

# force ownerships / permissions
# Everyone should have r+x permissions to SENSISION_DATA_DIR (targets, metrics)
RUN chown ${SENSISION_USER}:${SENSISION_GROUP} ${SENSISION_DATA_DIR} && \
    chmod 755 ${SENSISION_DATA_DIR}

RUN rm ${SENSISION_HOME}/bin/sensision.init
COPY sensision/sensision.init ${SENSISION_HOME}/bin/sensision.init
RUN chmod +x ${SENSISION_HOME}/bin/sensision.init


VOLUME ${WARP10_VOLUME}
VOLUME ${WARP10_MACROS}

RUN chown -R ${WARP10_USER}:${WARP10_GROUP} ${WARP10_HOME}/
# generate secret
RUN touch /app/code/warp10home/etc/conf.d/00-secrets.conf && \
  chmod +rw /app/code/warp10home/etc/conf.d/00-secrets.conf && \
  chown -R ${WARP10_USER}:${WARP10_GROUP} ${WARP10_CONFIG_DIR}
  # sudo ${WARP10_HOME}/etc/generate_crypto_key.py ${WARP10_SECRETS} && \

RUN chown -Rf cloudron:cloudron ${WARP10_DATA_DIR}
RUN chown -Rf cloudron:cloudron ${SENSISION_DATA_DIR}

RUN chmod +x ${WARP10_HOME}/bin/setup.sh
RUN chmod +x ${WARP10_HOME}/bin/warp10.start.sh

#log files 
# app/data/warp10home/logs/warpscript.log 
RUN touch ${WARP10_HOME}/logs/warpscript.log && \
  touch ${WARP10_HOME}/logs/nohup.log && \
  chmod -R +rw ${WARP10_HOME}/ && \
  chown -Rf cloudron:cloudron ${WARP10_HOME}/logs 

USER root
RUN touch ${WARP10_HOME}/templates/warp10-tokengen.mc2 && \
  chown -Rf cloudron:cloudron ${WARP10_HOME}/templates/ && \
  chmod +rw ${WARP10_HOME}/templates/warp10-tokengen.mc2

RUN WARP10_HOME_ESCAPED=$(echo ${WARP10_HOME} | sed 's/\\/\\\\/g' )  && \
  WARP10_HOME_ESCAPED=$(echo ${WARP10_HOME_ESCAPED} | sed 's/\&/\\&/g' ) && \
  WARP10_HOME_ESCAPED=$(echo ${WARP10_HOME_ESCAPED} | sed 's/|/\\|/g' ) && \
  LEVELDB_HOME_ESCAPED=$(echo ${LEVELDB_HOME} | sed 's/\\/\\\\/g' ) && \
  LEVELDB_HOME_ESCAPED=$(echo ${LEVELDB_HOME_ESCAPED} | sed 's/\&/\\&/g' ) && \
  LEVELDB_HOME_ESCAPED=$(echo ${LEVELDB_HOME_ESCAPED} | sed 's/|/\\|/g' ) && \
  sed -i${SED_SUFFIX} -e 's|^\(\s\{0,100\}\)WARP10_HOME=/opt/warp10-.*|\1WARP10_HOME='${WARP10_HOME_ESCAPED}'|' ${WARP10_HOME}/bin/snapshot.sh && \
  sed -i${SED_SUFFIX} -e 's|^\(\s\{0,100\}\)LEVELDB_HOME=${WARP10_HOME}/leveldb|\1LEVELDB_HOME='${LEVELDB_HOME_ESCAPED}'|' ${WARP10_HOME}/bin/snapshot.sh && \
  sed -i${SED_SUFFIX} -e 's|warpLog\.File=.*|warpLog.File='${WARP10_HOME_ESCAPED}'/logs/warp10.log|' ${WARP10_HOME}/etc/log4j.properties && \
  sed -i${SED_SUFFIX} -e 's|warpscriptLog\.File=.*|warpscriptLog.File='${WARP10_HOME_ESCAPED}'/logs/warpscript.log|' ${WARP10_HOME}/etc/log4j.properties && \
  chown -Rf cloudron:cloudron ${WARP10_HOME}/logs/ && \
  chmod -R +rw ${WARP10_HOME}/logs/ 

CMD ["/app/code/start.sh"]

EXPOSE 8081